var px = new PIXI.Application({
  width: 500,
  height: 300,
  antialias: true,
  transparent: true
});

document.querySelector('#main').appendChild(px.view);


var currentPos = {
  x: 0,
  y: 0
};

var currentPlayerPos = {
  x: 0,
  y: 0
};

var currentObjectPos = {
  x: 15,
  y: 15
};

var move = false;
var mT;
var player;

var levelData = {};

var win = {};

class Sq {
  constructor(stuff, win) {
    this.top = stuff.top
    this.left = stuff.left;
    this.right = stuff.right;
    this.bottom = stuff.bottom;
    this.win = win;
  }

  draw() {
    if (this.top) {
      let ln = new PIXI.Graphics();
      ln.lineStyle(6, 0x000, 1);
      ln.moveTo(currentPos.x, currentPos.y);
      ln.lineTo(currentPos.x + 30, currentPos.y);
      px.stage.addChild(ln);
    }
    if (this.left) {
      let ln = new PIXI.Graphics();
      ln.lineStyle(6, 0x000, 1);
      ln.moveTo(currentPos.x, currentPos.y);
      ln.lineTo(currentPos.x, currentPos.y + 30);
      px.stage.addChild(ln);
    }
    if (this.right) {
      let ln = new PIXI.Graphics();
      ln.lineStyle(3, 0x000, 1);
      ln.moveTo(currentPos.x + 30, currentPos.y);
      ln.lineTo(currentPos.x + 30, currentPos.y + 30);
      px.stage.addChild(ln);
    }
    if (this.bottom) {
      let ln = new PIXI.Graphics();
      ln.lineStyle(3, 0x000, 1);
      ln.moveTo(currentPos.x, currentPos.y + 30);
      ln.lineTo(currentPos.x + 30, currentPos.y + 30);
      px.stage.addChild(ln);
    }/*
    if(this.win) {
      let rct = tw.makeRectangle(currentPos.x + 18, currentPos.y + 15, 30, 30);
      rct.fill = '#3c17aa';
      rct.noStroke();
    }
    currentPos.x += 30;
    tw.update();
    */
  }
}

function loadLevel(levelStr) {
  let currentLine = 0;
  let currentx = 0;
  let g = levelStr.split('|');
  g.forEach(v => {
    if(v.startsWith('WIN:')) {
      v = v.replace('WIN:', '');
      win = {
        x: +v.split('$')[0],
        y: +v.split('$')[1]
      };
      return;
    }
    if (v === 'nL') {
      currentLine += 1;
      currentx = 0;
      currentPos.y += 30;
      currentPos.x = 0;
      return;
    }
    if (!(currentLine in levelData)) {
      levelData[currentLine] = [];
    }
    let u = v.split('$');
    let h = false;
    if(currentx == win.x && currentLine == win.y) h = true;
    levelData[currentLine].push({
      top: (u[0] == 'false') ? false : true,
      bottom: (u[1] == 'false') ? false : true,
      left: (u[2] == 'false') ? false : true,
      right: (u[3] == 'false') ? false : true
    });
    currentx += 1;
    u = [
      (u[0] == 'false') ? false : true,
      (u[1] == 'false') ? false : true,
      (u[2] == 'false') ? false : true,
      (u[3] == 'false') ? false : true
    ];
    new Sq({ top: u[0], bottom: u[1], left: u[2], right: u[3] }, h).draw();
  });
  player = new PIXI.Graphics();
  player.beginFill(0x000);
  player.drawCircle(0, 0, 7);
  player.endFill();
  player.x = 16;
  player.y = 16;
  px.stage.addChild(player);
  px.ticker.add(delta => gL(delta));
}

loadLevel('WIN:0$3|true$true$true$false|true$false$false$true|false$false$false$true|nL|false$false$true$false|false$true$false$true|false$false$false$true|nL|false$true$true$false|false$true$false$false|false$true$false$false|true$false$false$true|nL|false$true$true$false|false$true$false$false|false$true$false$false|false$true$false$true');

function gL(delta) {
  if(move) {
    switch(mT) {
      case 'up':
        player.y -= 1;
        break;
      case 'down':
        player.y += 1;
        break;
      case 'left':
        player.x -= 1;
        break;
      case 'right':
        player.x += 1;
    }
  };
}

function goTo(to) {
  if(move) return;
  mT = to;
  move = true;
  setTimeout(() => {
    move = false;
  }, 480)
}