var tw = new Two({ width: 500, height: 300 }).appendTo(document.querySelector('#main'));

var currentPos = {
  x: 0,
  y: 0
};

var currentPlayerPos = {
  x: 0,
  y: 0
};

var currentObjectPos = {
  x: 15,
  y: 15
};

var move = false;
var player;

var levelData = {};

var win = {};

var pg = [];

class Sq {
  constructor(stuff, win) {
    this.top = stuff.top
    this.left = stuff.left;
    this.right = stuff.right;
    this.bottom = stuff.bottom;
    this.win = win;
  }

  draw() {
    if (this.top) {
      let ln = tw.makeLine(currentPos.x, currentPos.y, currentPos.x + 30, currentPos.y);
      ln.stroke = '#000';
      ln.linewidth = 6;
    }
    if (this.left) {
      let ln = tw.makeLine(currentPos.x, currentPos.y, currentPos.x, currentPos.y + 30);
      ln.stroke = '#000';
      ln.linewidth = 6;
    }
    if (this.right) {
      let ln = tw.makeLine(currentPos.x + 30, currentPos.y, currentPos.x + 30, currentPos.y + 30);
      ln.stroke = '#000';
      ln.linewidth = 3;
    }
    if (this.bottom) {
      let ln = tw.makeLine(currentPos.x, currentPos.y + 30, currentPos.x + 30, currentPos.y + 30);
      ln.stroke = '#000';
      ln.linewidth = 3;
    }
    if(this.win) {
      let rct = tw.makeRectangle(currentPos.x + 18, currentPos.y + 15, 30, 30);
      rct.fill = '#3c17aa';
      rct.noStroke();
    }
    currentPos.x += 30;
    tw.update();
  }
}

function loadLevel(levelStr) {
  let currentLine = 0;
  let currentx = 0;
  let g = levelStr.split('|');
  g.forEach(v => {
    if(v.startsWith('WIN:')) {
      v = v.replace('WIN:', '');
      win = {
        x: +v.split('$')[0],
        y: +v.split('$')[1]
      };
      return;
    }
    //alert(JSON.stringify({currentx, currentLine}))
    if (v === 'nL') {
      currentLine += 1;
      currentx = 0;
      currentPos.y += 30;
      currentPos.x = 0;
      return;
    }
    if (!(currentLine in levelData)) {
      levelData[currentLine] = [];
    }
    let u = v.split('$');
    let h = false;
    if(currentx == win.x && currentLine == win.y) h = true;
    levelData[currentLine].push({
      top: (u[0] == 'false') ? false : true,
      bottom: (u[1] == 'false') ? false : true,
      left: (u[2] == 'false') ? false : true,
      right: (u[3] == 'false') ? false : true
    });
    currentx += 1;
    u = [
      (u[0] == 'false') ? false : true,
      (u[1] == 'false') ? false : true,
      (u[2] == 'false') ? false : true,
      (u[3] == 'false') ? false : true
    ];
    //alert(h)
    new Sq({ top: u[0], bottom: u[1], left: u[2], right: u[3] }, h).draw();
  });
  player = tw.makeCircle(15, 15, 7);
  player.fill = '#000';
  player.stroke = '#000';
  tw.update();
}


loadLevel('WIN:0$3|true$true$true$false|true$false$false$true|false$false$false$true|nL|false$false$true$false|false$true$false$true|false$false$false$true|nL|false$true$true$false|false$true$false$false|false$true$false$false|true$false$false$true|nL|false$true$true$false|false$true$false$false|false$true$false$false|false$true$false$true');

tw.bind('update', function(frameCount, timeDelta) {
  if (move) {
    switch (moveTo) {
      case 'up':
        player.translation.set(currentObjectPos.x, currentObjectPos.y - 29);
        var p = tw.makeLine(currentObjectPos.x, currentObjectPos.y, currentObjectPos.x, currentObjectPos.y - 29);
        p.stroke = '#000';
        pg.push(p);
        currentObjectPos.y -= 29;
        break;
      case 'left':
        player.translation.set(currentObjectPos.x - 27, currentObjectPos.y);
        var p = tw.makeLine(currentObjectPos.x, currentObjectPos.y, currentObjectPos.x - 27, currentObjectPos.y);
        p.stroke = '#000';
        pg.push(p);
        currentObjectPos.x -= 27;
        break;
      case 'right':
        player.translation.set(currentObjectPos.x + 27, currentObjectPos.y);
        var p = tw.makeLine(currentObjectPos.x, currentObjectPos.y, currentObjectPos.x + 27, currentObjectPos.y);
        p.stroke = '#000';
        pg.push(p);
        currentObjectPos.x += 27;
        break;
      case 'down':
        player.translation.set(currentObjectPos.x, currentObjectPos.y + 29);
        var p = tw.makeLine(currentObjectPos.x, currentObjectPos.y, currentObjectPos.x, currentObjectPos.y + 29);
        p.stroke = '#000';
        pg.push(p);
        currentObjectPos.y += 29;
    }
    (document.querySelector('#i1').checked) ? p.opacity = 1 : p.opacity = 0;
    move = false;
  }
});

function goTo(to) {
  switch (to) {
    case 'up':
      if (!levelData[currentPlayerPos.y][currentPlayerPos.x].top && currentObjectPos.y > 15) {
        if (typeof levelData[currentPlayerPos.y - 1][currentPlayerPos.x] == 'object' && !levelData[currentPlayerPos.y - 1][currentPlayerPos.x].bottom) {
          move = true;
          moveTo = 'up';
          tw.update();
          currentPlayerPos.y -= 1;
        }
      }
      break;
    case 'down':
      if (!levelData[currentPlayerPos.y][currentPlayerPos.x].bottom) {
        if (typeof levelData[currentPlayerPos.y + 1][currentPlayerPos.x] == 'object' && !levelData[currentPlayerPos.y + 1][currentPlayerPos.x].top) {
          move = true;
          moveTo = 'down';
          tw.update();
          currentPlayerPos.y += 1;
        }
      }
      break;
    case 'right':
      if (!levelData[currentPlayerPos.y][currentPlayerPos.x].right) {
        if (typeof levelData[currentPlayerPos.y][currentPlayerPos.x + 1] == 'object' && !levelData[currentPlayerPos.y][currentPlayerPos.x + 1].left) {
          move = true;
          moveTo = 'right';
          tw.update();
          currentPlayerPos.x += 1;
        }
      }
      break;
    case 'left':
      if (!levelData[currentPlayerPos.y][currentPlayerPos.x].left) {
        if (typeof levelData[currentPlayerPos.y][currentPlayerPos.x - 1] == 'object' && !levelData[currentPlayerPos.y][currentPlayerPos.x - 1].right) {
          move = true;
          moveTo = 'left';
          tw.update();
          currentPlayerPos.x -= 1;
        }
      }
  }
  if(currentPlayerPos.x == win.x && currentPlayerPos.y == win.y) {
    player.fill = '#fff';
    tw.update();
    document.querySelector('#i1').checked = true;
    document.querySelector('#i1').dispatchEvent(new Event('change'));
    document.body.style.pointerEvents = 'none';
    document.querySelector('#i1').style.display = 'none';
    setTimeout(() => 
    html2canvas(document.querySelector('#main')).then(c => {
      let h = document.createElement('img');
      h.style.width = 'auto';
      h.style.height = '300px';
      h.src = c.toDataURL();
      document.body.appendChild(h)
      tw.clear();
      tw.update();
      tw.makeText('OK', 250, 150);
      tw.update();
    }), 50);
  }
}

function conf(el) {
  if(el.id == 'i1') {
    if(el.checked) {
      pg.forEach(v => {
        v.opacity = 1;
        tw.update();
      });
    } else {
      pg.forEach(v => {
        v.opacity = 0;
        tw.update();
      });
    }
  }
}